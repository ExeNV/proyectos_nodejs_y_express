const passport=require('passport');
const localStrategy=require('passport-local').Strategy;
const Usuario= require('../models/usuario');
const googleStrategy=require('passport-google-oauth20').Strategy;
const facebookTokenStrategy=require('passport-facebook-token');

passport.use(new facebookTokenStrategy({
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET
},function(accessToken,refreshToken,profile,done){
    try{
        Usuario.findOneOrCreateByFacebook(profile,function(err,user){
            if(err) console.log('Err: '+err);
            return done(err,user);
        })
    }catch(errTry){
        console.log('ErrTry: '+errTry);
        return done(errTry,null); 
    }
}
))

passport.use(new localStrategy(
    function(email,password,done){
        Usuario.findOne({email:email},function(err,usuario){
            if(err) return done(err);
            if(!usuario) return done(null,false,{message: "Email innexistente o incorrecto"});
            if(!usuario.validPassword(password)) return done(null,false,{message: "El password es incorrecto"});
            
            return done(null,usuario);
        })
    }
));

passport.use(new googleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: `${process.env.HOST}/auth/google/callback`
},function(request,accessToken,refreshToken,profile,cb){
    console.log(profile);
    Usuario.findOneOrCreateByGoogle(profile,function(err,user){
        return cb(err,user);
    })
}))


passport.serializeUser(function(user,cb){
    cb(null,user.id);
})

passport.deserializeUser(function(id,cb){
    Usuario.findById(id,function(err,usuario){
        cb(err,usuario);
    })
});


module.exports=passport;