const mongoose=require('mongoose');
const { token } = require('morgan');
const Schema=mongoose.Schema;

const tokenSchema=new Schema({
    _userId: {type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'},
    token: {required: true, type: String},
    createdat:{type: Date, required: true, default: Date.now, expires: 43200}
});

module.exports=mongoose.model('Token',tokenSchema);