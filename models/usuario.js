var mongoose= require('mongoose');
var Schema= mongoose.Schema;
var Reserva= require('./reserva');
var crypto= require('crypto');

const saltRounds=10;
const bcrypt=require('bcrypt');
const uniqueValidator= require('mongoose-unique-validator');

const Token= require('../models/token');
const mailer=require('../mailer/mailer');

//VALIDAR EMAIL
const ValidateEmail= function(email){
    const re= /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    return re.test(email);
}

var usuarioSchema= new Schema({
    nombre: {
        type: String, 
        trin: true,
        required:[true,"El nombre es obligatorio"]
    },
    email: {
        type: String,
        trin: true,
        required:[true,"El email es obligatorio"],
        lowercase: true,
        unique:true,
        validate:[ValidateEmail,"Por favor ingrese un email válido"],
        match: /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i
    },
    password:{
        type: String,
        trin: true,
        required:[true, "El password es obligatorio"]
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator,{message: 'El {PATH} ya existe con otro usuario'});


usuarioSchema.pre('save',function(next){
    if(this.isModified('password')){
        this.password=bcrypt.hashSync(this.password,saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword=function(password){
    return bcrypt.compareSync(password,this.password);
}

usuarioSchema.methods.reservar=function(biciCode, desde, hasta, cb){
    var reserva= new Reserva({usuario: this._id, bicicleta: biciCode, desde: desde, hasta: hasta});

    console.log(reserva);

    reserva.save().then(cb);//¿Why?
};

usuarioSchema.methods.enviar_email_bienvenida=function(){
    const token= new Token({_userId: this._id, token: crypto.randomBytes(16).toString('hex')});
    const email_destino= this.email;

    token.save(function(err){
        if(err){return console.log(err.message)};

        const mailoptions={
            from: 'no-reply@resbicicletas.com',
            to: email_destino,
            subject: 'Verificación de cuenta',
            text: 'Hola \n\n' + 'Por favor, para verificar su cuenta haga click en este enlace: \n'+`http://localhost:5000/token/confirmation/${token.token}`,
        };
    
        mailer.sendMail(mailoptions,function(err){
            if(err){ return console.log(err.message)}
    
            console.log(`Un correo de verificación fue enviado a: ${email_destino}`);
        })
    })
}

usuarioSchema.methods.resetPassword=function(cb){
    const token= new Token({_userId: this._id, token: crypto.randomBytes(16).toString('hex')});
    const mail_destino=this.email;

    token.save(function(err){
        if(err){return cb(err)};

        const mailOptions={
            from: 'no-reply@redbicicletas.com',
            to: mail_destino,
            subject: 'Restablecer contraseña',
            text:'Hola \n\n' + 'Por favor, para restablecer la contraseña de su cuenta haga click en este enlace: \n'
            +   `http://localhost:5000/resetPassword/${token.token}`,
        };

        mailer.sendMail(mailOptions,function(err){
            if(err){return cb(err)};

            console.log(`Se ha enviado un correo de recuperación de contraseña a: ${mail_destino}`);
            
        })

        cb(null);
    })
}

usuarioSchema.statics.findOneOrCreateByGoogle=function findOneOrCreate(condition,callback){
    const self=this;
    console.log(condition);
    self.findOne({
        $or:[{'googleId': condition.id},{'email':condition.emails[0].value}]
    },(err,result)=>{
       if(result){
           callback(err,result);
       }else{
           console.log('----------CONDITION----------');
           console.log(condition);
           let values={};
           values.googleId=condition.id;
           values.email=condition.emails[0].value;
           values.nombre=condition.displayName || 'SIN NOMBRE';
           values.verificado=true;
           values.password=condition._json.sub;
           console.log('----------VALUES----------');
           console.log(values);

           self.create(values,(err,result)=>{
               if(err){console.log(err)};
               return callback(err,result);
           });

       }
    })
}

usuarioSchema.statics.findOneOrCreateByFacebook=function findOneOrCreate(condition,callback){
    const self=this;
    console.log(condition);
    self.findOne({
        $or:[{'facebookId': condition.id},{'email':condition.emails[0].value}]
    },(err,result)=>{
       if(result){
           callback(err,result);
       }else{
           console.log('----------CONDITION----------');
           console.log(condition);
           let values={};
           values.facebookId=condition.id;
           values.email=condition.emails[0].value;
           values.nombre=condition.displayName || 'SIN NOMBRE';
           values.verificado=true;
           values.password=crypto.randomBytes(16).toString('hex');
           console.log('----------VALUES----------');
           console.log(values);

           self.create(values,(err,result)=>{
               if(err){console.log(err)};
               return callback(err,result);
           });

       }
    })
}

module.exports= mongoose.model('Usuario',usuarioSchema);