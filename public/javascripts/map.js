var map = L.map('mapid').setView([-26.8498388, -65.1788607], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    accessToken: 'pk.eyJ1IjoiZXhlbnYiLCJhIjoiY2tpd2FhNHZsMGhscTJzcDNvOGwxZ2QzZCJ9.6bd-DlcEoc77tbdMpmN6xA'
}).addTo(map);

L.marker([-26.832742, -65.194611],{title: "marcador no bici"}).addTo(map);
L.marker([-26.820683, -65.191582],{title: "marcador no bici"}).addTo(map);

$.ajax({
    dataType: "json",
    url:"api/bicicletas",
    success: function(result){
        console.log(result)
        var bicicletas= result.bicicletas;
        for(bici of bicicletas){
            L.marker(bici.ubicacion,{title: `Bici con ID: ${bici.id}`}).addTo(map);
        }
    }
})