var Bicicletas= require('../models/bicicleta');

exports.bicicleta_list=function(req,res){
    Bicicletas.allBicis(function(err,bicis){
        res.render('bicicletas/index',{bicis: bicis});
    });
}

exports.bicicleta_create_get=function(req,res){
    res.render('bicicletas/create');
}

exports.bicicleta_create_post=function(req,res){
    var bici= new Bicicletas({
        code: req.body.code, color: req.body.color, 
        modelo: req.body.modelo, 
        ubicacion: [req.body.lat, req.body.lng]
    });

    Bicicletas.add(bici,function(err,newBici){
        res.redirect("/bicicletas");
    });

}

exports.bicicleta_update_get=function(req,res){
    Bicicletas.findByCode(req.params.code,function(err,bici){
        res.render('bicicletas/update',{bici});
    });
};

exports.bicicleta_update_post=function(req,res){
    Bicicletas.removeByCode(req.params.code,function(err){
        var bici =new Bicicletas({code: req.body.code, 
            color: req.body.color,
            modelo: req.body.modelo,
            ubicacion: [req.body.lat,req.body.lng]
        });

        Bicicletas.add(bici,function(err,newBici){
            res.redirect('/bicicletas');
        })
        
    })
};

exports.bicicleta_delete_post=function(req,res){
    Bicicletas.removeByCode(req.params.code,function(err){
        res.redirect("/bicicletas");
    });
}