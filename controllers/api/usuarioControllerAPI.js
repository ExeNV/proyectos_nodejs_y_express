var Usuario= require('../../models/usuario');

exports.usuario_list=(req,res)=>{
    Usuario.find({},(err,usuarios)=>{
        res.status(200).json({
            usuarios: usuarios,
        });
    });
};//fin de usuario_list

exports.usuario_create=(req,res)=>{
    var usuario= new Usuario({nombre: req.body.nombre, email: req.body.email, password: req.body.password});

    usuario.save((err)=>{
        res.status(200).json({
            usuario: usuario,
        });
    });
};//fin de usuario_create


exports.usuario_reservar=(req,res)=>{
    Usuario.findById(req.body.usuario_id,(err,usuario)=>{
        console.log("El usuario encontrado es: ", usuario);

        usuario.reservar(req.body.bici_id,req.body.desde, req.body.hasta,(err,reserva)=>{
            console.log("RESERVA COMPLETA!");

            res.status(200).json({
                reserva_creada: reserva,
            });
        });
    });
};//fin de usuario_reservar


