var Bicicleta= require('../../models/bicicleta');

exports.bicicleta_list=function(req,res){
    
    Bicicleta.allBicis((err,bicis)=>{
        if(err) console.log(err);
        res.status(200).json({
            bicicletas: bicis,
        });
    });
    
    /*res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })*/
}

exports.bicicleta_create=function(req,res){
    var bici = Bicicleta.createInstance(req.body.id, req.body.color,req.body.modelo, [req.body.lat, req.body.lng])
    res.status(200).json({
        biciCreada: bici,
    });
}

exports.bicicleta_delete=function(req,res){
    var codeInt= parseInt(req.params.code);
    Bicicleta.removeByCode(codeInt,function(err){
        if(err) console.log(err);
        res.status(204).send();
    });

}

exports.bicicleta_update=function(req,res){
    Bicicleta.removeByCode(req.params.code,function(err){
        var biciDatosMod= Bicicleta.createInstance(req.body.id, req.body.color,req.body.modelo, [req.body.lat, req.body.lng])
        res.status(200).json({
            bici_modificada: biciDatosMod,
        });
    });
}
