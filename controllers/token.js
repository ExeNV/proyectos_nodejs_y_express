const Usuario=require('../models/usuario');
const Token=require('../models/token');

module.exports={
    confirmationGet: function(req,res,next){
        Token.findOne({token: req.params.token},function(err,token){
            if(!token) return res.status(400).send({type: 'not-verified', msg: 'No encontramos el token solicitado, quizá haya expirado solicítelo nuevamente'})
            Usuario.findById(token._userId,function(err,usuario){
                if(!usuario) return res.status(400).send({msg: 'No encontramos el usuario solicitado'});
                if(usuario.verificado) res.redirect('/usuarios');
                usuario.verificado=true;
                usuario.save(function(err){
                    if(err) return res.status(500).send({msg: err.message});
                    res.redirect('/');
                })
            })
        });
    }
};
