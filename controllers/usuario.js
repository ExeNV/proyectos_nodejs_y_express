var Usuario= require('../models/usuario');

exports.usuario_list=function(req,res){
     Usuario.find({},function(err,usuarios){
         res.render('usuarios/',{usuarios: usuarios});
     })
}
exports.usuario_create_get=function(req,res){
    res.render('usuarios/create',{errors:{},usuario: new Usuario({})});
};

exports.usuario_create_post=function(req,res){
    if(req.body.password!=req.body.confirm_password){
        res.render('usuarios/create',{errors: {confirm_password: {message: "No coincide con el password ingresado"}},usuario: new Usuario({nombre: req.body.nombre, email: req.body.email, password: req.body.password})})
        return;
    }

    var newUsuario= new Usuario({
        nombre: req.body.nombre,
        email: req.body.email,
        password: req.body.password
    });

    Usuario.create(newUsuario,function(err,usuarioNuevo){
        if(err){
            console.log(err);
            res.render('usuarios/create',{errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
        }else{
            usuarioNuevo.enviar_email_bienvenida();
            res.redirect('/usuarios');
        }
    })
}


exports.usuario_update_get=function(req,res){
    Usuario.findById(req.params.user_id,function(err,usuario){
        res.render('usuarios/update',{errors:{},usuario: usuario});
    });
}

exports.usuario_update_post=function(req,res){
    
    Usuario.findByIdAndUpdate(req.params.user_id,{nombre: req.body.nombre},function(err,usuario){
        if(err){
            console.log(err);
            res.render('usuarios/update',{errors:err.errors,usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
        }else{
            res.redirect('/usuarios');
        }
    });
};

exports.usuario_delete_post=function(req,res){
    Usuario.findByIdAndRemove(req.params.user_id,function(err){
        res.redirect('/usuarios');
    });
}
