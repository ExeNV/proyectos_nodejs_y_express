require('newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport= require('./config/passport');
const session= require('express-session');
const MongoDbStore= require('connect-mongodb-session')(session);
const jwt= require('jsonwebtoken');


const Usuario=require('./models/usuario');
const Token=require('./models/token');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var tokenRouter= require('./routes/token');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter= require('./routes/api/usuarios');
var usuariosRouter=require('./routes/usuarios');
var authAPIRouter=require('./routes/api/auth');

let Store;

if(process.env.NODE_ENV==='development'){
  Store=new session.MemoryStore;
}else{
  Store=new MongoDbStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  })
  Store.on('error',function(err){
    assert.ifError(err);
    assert.ok(false);
  })
}

var app = express();

app.set('secretKey','askjfkasjf1kjkafjakj');

app.use(session({
  cookie:{maxAge: 240*60*60*1000},
  store: Store,
  saveUninitialized: true,
  resave:'true',
  secret:'kasjfj1kk1j1b1j1jksdaosad!!&i%a&&sln' 
}));

//Conectando la DB------------------------------------------------------//
var mongoose= require('mongoose');
const { assert } = require('console');
//var mongoDB= 'mongodb://localhost/red-bicicletas';
var mongoDB= process.env.MONGO_URI;
/*l5oS6yMZFI4ESWgM*/

mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.set('useCreateIndex', true);
mongoose.Promise=global.Promise;
var db= mongoose.connection;
db.on('error',console.error.bind(console,'MongoDB conection error: '));
/*----------------------------------------------------------------------*/

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

////////////////////////////////////////////////////////////////////////////////
/*RUTAS DE LOGIN, LOGOUT y RESET PASSWORD*/
app.get('/login',function(req,res){
  res.render('session/login',{errors:{},usuario:{}})
});

app.post('/login',function(req,res,next){
  //passport
  passport.authenticate('local',function(err,usuario,info){
    if(err) return next(err);
    if(!usuario) return res.render('session/login',{info});

    req.logIn(usuario,function(err){
      if(err){return next(err);}
      return res.redirect('/');
    })
  })(req,res,next);
});

app.get('/logout',function(req,res){
  req.logOut();
  res.redirect('/login');
});

app.get('/forgotPassword',function(req,res){
  res.render('session/forgotPassword');

});

app.post('/forgotPassword',function(req,res){
  Usuario.findOne({email: req.body.email},function(err,usuario){
    if(!usuario){return res.render('session/forgotPassword',{info:{message: 'El email no coincide con el de ningún usuario registrado'}})};

    usuario.resetPassword(function(err){
      if(err){return next(err);}
      console.log('session/forgotPasswordMessage');
    })

    res.render('session/forgotPasswordMessage');

  });
});

app.get('/resetPassword/:token',function(req,res,next){
  Token.findOne({token: req.params.token},function(err,token){
    if(!token){return res.status(400).send({type: 'not-verified', msg: 'No encontramos el token solicitado'})};
    
    Usuario.findById(token._userId,function(err,usuario){
        if(!usuario) return res.status(400).send({msg: 'No encontramos el usuario solicitado asociado al token'});
        
        res.render('session/resetPassword',{errors:{},usuario: usuario});
    });
  })
});

app.post('/resetPassword',function(req,res){
  if(req.body.password!=req.body.confirm_password){
    res.render('session/resetPassword',{errors:{confirm_password:{message:'No coincide con el password ingresado'}},
  usuario: new Usuario({email: req.body.email})});

  return;

  }

  Usuario.findOne({email: req.body.email},function(err,usuario){
    usuario.password=req.body.password;

    usuario.save(function(err){
      if(err) return res.render('session/resetPassword',{errors: err.errors, usuario: new Usuario({email: req.body.email})});
      else{
        res.redirect('/login');
      }
    })
  })
})
///////////////////////////////////////////////////////////////////////////////////////////////


app.use('/users', usersRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/bicicletas',validateUser,bicicletasAPIRouter);
app.use('/api/usuarios',validateUser,usuariosAPIRouter);
app.use('/usuarios',usuariosRouter);
app.use('/token',tokenRouter);
app.use('/api/auth',authAPIRouter);

app.use('/privacy_policy',function(req,res){
  res.sendFile('public/privacy_policy.html');  
})

app.use('/googled8f15d6e4573b304',function(req,res){
  res.sendFile('public/googled8f15d6e4573b304.html');  
})

app.get('/auth/google',
  passport.authenticate('google',{scope:[
    'email','profile'] } )
)

app.get('/auth/google/callback',passport.authenticate('google',{
  successRedirect:'/',
  failureRedirect:'/error'})
);


app.use('/', loggedIn, indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


function loggedIn(req,res,next){
  if(req.user){
    next();

  }else{
    console.log('usuario sin loguearse');
    res.redirect('/login');
  }
}

function validateUser(req,res,next){
  jwt.verify(req.headers['x-access-token'],req.app.get('secretKey'),function(err,decoded){
    if(err){
      res.json({status: 'error',message: err.message, data:null});
    }else{
      req.body.userId=decoded.id;
      console.log(req.body);
      //console.log(decoded);
      next();
    }
  })
}

module.exports = app;
