var express=require('express');
var router= express.Router();

var usuarioController= require('../controllers/usuario');

router.get('/',usuarioController.usuario_list);
router.get('/create',usuarioController.usuario_create_get);
router.post('/create',usuarioController.usuario_create_post);
router.get('/:user_id/update',usuarioController.usuario_update_get);
router.post('/:user_id/update',usuarioController.usuario_update_post);
router.post('/:user_id/delete',usuarioController.usuario_delete_post);
/*
router.get('/login',usuarioController.usuario_login_get);
router.post('/login',usuarioController.usuario_login_post);
router.get('/login/rec_password',usuarioController.usuario_rec_password_get);
router.post('/login/rec_password',usuarioController.usuario_rec_password_post);
router.post('/login/reset_password',usuarioController.usuario_reset_password);
*/
module.exports=router;