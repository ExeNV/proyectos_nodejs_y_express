var mongoose= require('mongoose');
var Bicicleta= require('../../models/bicicleta');


describe('Testing bicicletas',()=>{
    
    beforeAll(function(done) {

        mongoose.connection.close().then(() => {

            var mongoDB = 'mongodb://localhost:27017/testdb';

            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

            mongoose.set('useCreateIndex', true);
            
            var db = mongoose.connection;

            db.on('error', console.error.bind(console, 'MongoDB connection error: '));

            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });
        });
    });//termina el BeforeAll

    afterEach((done)=>{
        Bicicleta.deleteMany({},function(err){
            if(err) console.log(err);

            done();
        })
    });//termine el afterEach
    
    describe('Bicicleta.createInstance',()=>{
        it('Crea una instancia de bicicleta: ',(done)=>{
            
            var bici= Bicicleta.createInstance(1,"verde","montain",[-26,-64]);
    
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("montain");
            done();
        });
    });//termina el testing creación de instancias

    describe('Bicicleta.allBicis',()=>{
        it('Comienza vacio',(done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });//termina el test allBicis

    describe('Bicicleta.add',()=>{
        it('Agrega una bicicleta',(done)=>{
            var aBici= new Bicicleta({code: 1,color: "verde",modelo: "montaña",ubicacion: [-26.8498387, -65.1788606]});
            Bicicleta.add(aBici,(err,newBici)=>{
                if(err) console.log(err);
                Bicicleta.allBicis((err,bicis)=>{
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });//termina el test Bicicleta.add

    describe('Bicicleta.findByCode',()=>{
        it('Devuelve una bici con code 1: ',(done)=>{
            var aBici= new Bicicleta({code: 1, color: "verde", modelo: "urbana"});

            Bicicleta.add(aBici,(err,newBici)=>{
                if(err) console.log(err);

                var aBici2= new Bicicleta({code: 2, color: "rojo", modelo: "montaña"});

                Bicicleta.add(aBici2,(err,newBici)=>{
                    if(err) console.log(err);

                    Bicicleta.findByCode(1,(err,tarjetBici)=>{
                        expect(tarjetBici.code).toBe(aBici.code);
                        expect(tarjetBici.color).toBe(aBici.color);

                        done();
                    });
                });
            });
        });
    });//termina el test Bicicleta.findByCode

    describe('Bicicleta.removeByCode',()=>{
        it('Elimina la bicicleta con code 1', function(done){
            var aBici= new Bicicleta({code: 1, color: "verde", modelo: "urbana"});

            Bicicleta.add(aBici,(err,newBici)=>{
                if(err) console.log(err);

                var aBici2= new Bicicleta({code: 2, color: "rojo", modelo: "montaña"});

                Bicicleta.add(aBici2,(err,newBici)=>{
                    if(err) console.log(err);

                    Bicicleta.removeByCode(1,(err)=>{
                        if(err) console.log(err);

                        Bicicleta.allBicis((err,bicis)=>{
                            expect(bicis.length).toBe(1);
                            done();
                        })
                    });
                });
            });
            //Elimino la bici con code: 1
           

        });
    });//termina el test Bicicleta.removeByCode


});//termina el testing bicicletas
