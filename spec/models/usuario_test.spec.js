var mongoose= require('mongoose');
var Bicicleta= require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva= require('../../models/reserva');


describe('Testing Usuarios',()=>{
    
    beforeAll(function(done) {

        mongoose.connection.close().then(() => {

            var mongoDB = 'mongodb://localhost/testdb';

            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

            mongoose.set('useCreateIndex', true);

            var db = mongoose.connection;

            db.on('error', console.error.bind(console, 'MongoDB connection error: '));

            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });
        });
    });//termina el BeforeAll

    afterEach((done)=>{
        Reserva.deleteMany({},function(err,success){
            if(err) console.log(err);

            Usuario.deleteMany({},(err,success)=>{
                if(err) console.log(err);

                Bicicleta.deleteMany({},(err,success)=>{
                    if(err) console.log(err);
                    done();
                })
            })
        });
    });//termine el afterEach
    
    describe('Usuario.reservar',()=>{
        it('Se crea la reserva',(done)=>{
            var usuario= new Usuario({nombre: 'Exequiel'});

            var bicicleta= new Bicicleta({code: 1, color: "rojo", modelo: "urbana", ubicacion: []});
            usuario.save().then(
                bicicleta.save()
            );

            var hoy = new Date();
            var mañana= new Date();
            mañana.setDate(hoy.getDate() + 1);

            usuario.reservar(bicicleta.id,hoy,mañana,(err,reserva)=>{
                Reserva.find({}).populate('bicicleta').populate('usuario').exec((err,reservas)=>{
                    //console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);

                    done();

                });
            })
        });
    });// termina el testing Usuario.reservar

    


});//termina el testing usuarios