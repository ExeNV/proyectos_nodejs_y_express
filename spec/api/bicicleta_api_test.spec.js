var Bicicleta= require('../../models/bicicleta');
var mongoose= require('mongoose');
var request= require('request');

var server= require('../../bin/www');
const { response } = require('express');
const { RequestTimeout } = require('http-errors');

describe('Bicicleta API',()=>{
    
    beforeAll(function(done) {

        mongoose.connection.close().then(() => {

            var mongoDB = 'mongodb://localhost/testdb';

            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

            mongoose.set('useCreateIndex', true);

            var db = mongoose.connection;

            db.on('error', console.error.bind(console, 'MongoDB connection error: '));

            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });
        });
    });//termina el BeforeAll

    afterEach((done)=>{
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            done();
        });
    });//termine el afterEach
    

     //URL DE LA API
     const url='http://localhost:5000/api/bicicletas';
     /////////////////////////////////////////////////

    describe('GET BICICLETAS /',()=>{
        it('STATUS 200',(done)=>{
           
           Bicicleta.allBicis((err,bicis)=>{
            if(err) console.log(err)    
            expect(bicis.length).toBe(0);
           });
           
           var a= new Bicicleta({code: 1,color: "negro",modelo: "urbana",ubicacion: [-26.8498387, -65.1788606]});

           Bicicleta.add(a,(err,newBici)=>{
               if(err) console.log(err)

               Bicicleta.allBicis((err,bicis)=>{
                   expect(bicis.length).toBe(1);
               })
           });

           request.get(url,(error,response,body)=>{
            const data= JSON.parse(body);   

            expect(response.statusCode).toBe(200);
            expect(data.bicicletas[0].code).toEqual(1);
            expect(data.bicicletas[0].color).toBe("negro");
            done();
           });

        });

    }); //TERMINA EL TEST DE: GET BICICLETAS /

    describe('POST BICICLETAS /create',()=>{
        it('STATUS 200',(done)=>{
            var headers= {'content-type' : 'application/json'}
            var aBici='{"id": 10, "color": "rojo", "modelo": "urbana", "lat": -26.84, "lng": -65.17}'

            request.post({
                headers: headers,
                url: url+'/create',
                body: aBici
            },(error,response,body)=>{
                var data= JSON.parse(body);

                expect(response.statusCode).toBe(200);
                expect(data.biciCreada.code).toEqual(10);
                expect(data.biciCreada.color).toBe("rojo");
                expect(data.biciCreada.modelo).toBe("urbana");

                done();
                
            });//termina el post
        });
    });//TERMINA EL TEST DE: POST BICICLETAS /create

    describe('POST BICICLETAS /:code/update',()=>{
        it('STATUS 200',(done)=>{
            
            Bicicleta.allBicis((err,bicis)=>{
                if(err) console.log(err)    
                expect(bicis.length).toBe(0);
            });

            var a= new Bicicleta({
                code: 10,
                color: "rojo",
                modelo: "montain-bike",
                ubicacion: []
            });
            
            Bicicleta.add(a,(err,newBici)=>{
                if(err) console.log(err);

                Bicicleta.allBicis(function(err,bicis){
                    if(err) console.log(err);

                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toEqual(10);
                    expect(bicis[0].color).toBe("rojo");
                    expect(bicis[0].modelo).toBe("montain-bike");

                });
            });

            

            var headers= {'content-type' : 'application/json'}
            var jsonBici= '{"id": 10, "color": "dorado", "modelo": "urbana", "lat": -26.84, "lng": -65.17}';
            
            request.post({
                headers: headers,
                url: url+`/${jsonBici.id}/update`,
                body: jsonBici
            },(error, response, body)=>{
                const data= JSON.parse(body);

                expect(response.statusCode).toBe(200);
                expect(data.bici_modificada.code).toEqual(10);
                expect(data.bici_modificada.color).toBe("dorado");
                expect(data.bici_modificada.modelo).toBe("urbana");
                done();
            });



        });
    });//TERMINA EL TEST DE: POST BICICLETAS /:id/update

    describe('POST BICICLETAS /:code/delete',()=>{
        it('STATUS 204',(done)=>{
            
            Bicicleta.allBicis((err,bicis)=>{
                expect(bicis.length).toBe(0);
            });
            
            var a= new Bicicleta({
                code: 10,
                color: "rojo",
                modelo: "montain-bike",
                ubicacion: []
            })

            Bicicleta.add(a,(err,newBici)=>{
                Bicicleta.allBicis((err,bicis)=>{
                    expect(bicis.length).toBe(1);
                })
            })

            var headers= {'content-type' : 'application/json'}

            request.post({
                headers: headers,
                url: url+'/10/delete',
            },(err,response,body)=>{
                expect(response.statusCode).toBe(204);

                Bicicleta.allBicis(function(err,bicis){
                    if(err) console.log(err);
                    expect(bicis.length).toBe(0);
                    done();
                });
            })

        });
    });// TERMINA EL TEST DE: POST BICICLETAS /:id/delete

});//TERMINA EL TEST DE TODA LA API BICICLETAS