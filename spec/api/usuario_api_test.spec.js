var mongoose= require('mongoose');
var servidor= require('../../bin/www');
var request= require('request');

var Bicicleta= require('../../models/bicicleta');
var Usuario = require('../../models/usuario');
var Reserva= require('../../models/reserva');

const { response } = require('express');
const { head } = require('../../routes/api/usuarios');

describe('Testint API USUARIO',function(){
    beforeAll(function(done) {

        mongoose.connection.close().then(() => {

            var mongoDB = 'mongodb://localhost/testdb';

            mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

            mongoose.set('useCreateIndex', true);

            var db = mongoose.connection;

            db.on('error', console.error.bind(console, 'MongoDB connection error: '));

            db.once('open', function () {
                console.log('We are connected to test database!');
                done();
            });
        });
    });//termina el BeforeAll

    afterEach((done)=>{
        Reserva.deleteMany({},function(err,success){
            if(err) console.log(err);

            Usuario.deleteMany({},(err,success)=>{
                if(err) console.log(err);

                Bicicleta.deleteMany({},(err,success)=>{
                    if(err) console.log(err);
                    done();
                })
            })
        });
    });//termine el afterEach

    //URL DE LA API USUARIO
    const url= 'http://localhost:5000/api/usuarios'
    ///////////////////////////////////////////////

    describe('Listar usuarios GET /',function(){
        it('Tiene que haber dos usuarios',(done)=>{
            var a= new Usuario({nombre: "Exequiel"});
            var b= new Usuario({nombre: "Cristian"});
            a.save().then(
                b.save());
            

            request.get(url,function(err,response,body){
                const data= JSON.parse(body);

                expect(response.statusCode).toBe(200);
                expect(data.usuarios.length).toBe(2);
                done();
            })
        });
    });//fin de test GET /

    describe('Crear usuario POST /create',()=>{
        it('Crear un usuario de nombre Pedro',(done)=>{
            var jsonUsuario='{"nombre": "Pedro"}';
            var headers= {"content-type": "application/json"};

            request.post({
                headers: headers,
                url: url+'/create',
                body: jsonUsuario,
            },(err,response,body)=>{
                const data= JSON.parse(body);

                expect(response.statusCode).toBe(200);
                expect(data.usuario.nombre).toEqual("Pedro");
                done();
            })
        });

    });//fin de test POST /create

    describe('Crear reserva POST /reservar',()=>{
        it('se crea una reserva',(done)=>{
            var usuario= new Usuario({nombre: 'Exequiel'});

            var bicicleta= new Bicicleta({code: 1, color: "rojo", modelo: "urbana", ubicacion: []});

            var hoy = new Date();
            var mañana= new Date();
            mañana.setDate(hoy.getDate() + 1);

            var headers= {"content-type" : "application/json"};
            var jsonData;

            usuario.save().then(
                bicicleta.save().then(
                    jsonData=`{"usuario_id": ${usuario._id},"bici_id": ${bicicleta._id}, "desde":${hoy}, "hasta":${mañana},}`,
                    
                    request.post({
                        headers: headers,
                        url: url+'/reservar',
                        body: jsonData,
                    },(err,response,body)=>{
                        
                        done()
                    })
                )
            );

            

                
        });

    });//fin de test POST /reservar

});//fin de Testing API USUARIO